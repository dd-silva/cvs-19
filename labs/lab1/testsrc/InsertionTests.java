import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class InsertionTests {

    @Test
    public void testPush() {
        final MyIntegerList mil = new MyIntegerList();

        assertEquals(0, mil.size());
        assertEquals("[]", mil.toString());

        mil.push(1);
        assertEquals(1, mil.size());
        assertNotEquals("[]", mil.toString());
        assertEquals("[1]", mil.toString());

        mil.push(2);
        assertEquals(2, mil.size());
        assertEquals("[1,2]", mil.toString());

        mil.push(1);
        assertEquals(3, mil.size());
        assertEquals("[1,2,1]", mil.toString());
    }

    @Test
    public void testSortedInsertion(){
        final MyIntegerList mil = new MyIntegerList();

        assertEquals(0, mil.size());
        assertEquals("[]", mil.toString());

        mil.push(4);
        mil.push(5);
        mil.push(6);

        assertEquals(3, mil.size());
        assertEquals("[4,5,6]", mil.toString());

        mil.sortedInsertion(1);
        assertEquals(4, mil.size());
        assertEquals("[1,4,5,6]", mil.toString());

        mil.sortedInsertion(3);
        assertEquals(5, mil.size());
        assertEquals("[1,3,4,5,6]", mil.toString());

        mil.sortedInsertion(3);
        assertEquals(6, mil.size());
        assertEquals("[1,3,3,4,5,6]", mil.toString());

        mil.sortedInsertion(0);
        assertEquals(7, mil.size());
        assertEquals("[0,1,3,3,4,5,6]", mil.toString());

        mil.sortedInsertion(-2);
        assertEquals(8, mil.size());
        assertEquals("[-2,0,1,3,3,4,5,6]", mil.toString());

        mil.sortedInsertion(-1);
        assertEquals(9, mil.size());
        assertEquals("[-2,-1,0,1,3,3,4,5,6]", mil.toString());



    }

}