/*
    Class Drone 

    Construction and Verification of Software, FCT-UNL, © (uso reservado)

    This class emulates a drone. The drone has to obey some restrictions. 
    There are restrictions on its speed and positionition, and on the conditions 
    its operations can execute.

    The drone supports 4 operations: take off, move, addWaypoint, and land.
    Take note that for every operation, you'll need to implement a method
    for starting it and another for finishing it. For instance, let us
    consider the operation move which moves the drone from point A to
    point B. This operation is divided into <Move> and <CompleteMove>.
    In <Move>, it is ncessary to set the target destination and the drone's
    speed. In the second part of the operations, <CompleteMove>, it is
    necessary to set the drone's positionition as being the target positionition
    (it arrived to the target destination) and set the speed of the drone
    to zero. It is hovering at the target positionition.

    The drone can freely move inside a semi-sphere with
    radius <r> with center <n> meters above its control station and and in a
    cilinder with a height of <n> meters and whose base is centered in the
    drone's control station. Regarding the speed of the drone, the drone's 
    maximum speed is 10 Km/h.

    <r> -> radius
    <n> -> operational height

    OPERATIONS:
    
        Take Off
        
        Makes the drone climb to its operational height at maximum speed and hover
        at that positionition.
        With the exception for its height, its positionition must not change.
        This operation can only execute successfully if the drone is on the ground
        (landed).


        Move

        Given a valid positionition, this operation causes the drone to move to the
        target positionition. When moving, the drone moves at its maximum speed.
        In a similar fashion to the previous operation, this operation only
        executes if the drone is idle.


        AddWaypoint

        Given a positionition IN THE SEMI-SPHERE with center at <n> meters above its
        control station, the drone will add that positionition to a sequence of
        positionitions it will follow one by one. When following waypoints, the
        drone is said to be patrolling and it moves at half its maximum
        speed. It is only positionsible to add new waypoints to the drone if
        it is hovering (idle) or it is already on paatrol.


        Land

        Lands the drone directly below its current positionition. When landing,
        the drone does so at its maximum speed. This operation can only execute
        when the drone is idle.

    When instantiating a new drone, it is necessary to supply its
    operational height (<n>) and its operation range (<r>).

    There is a mapping between the representation state and abstract state
    that guarantees the soundness of the ADT. Also, don't forget the
    state invariant.
	
	The delivery date is the 29th of April.
 */

class Drone {

    const LANDED := 0;
    const MOVING := 1;
    const IDLE := 2;
    const PATROLLING := 3;
    const MAXSPEED := 10;    

    var n:int;
    var r:int;
    var position:(int, int, int);
    var targetpos:(int, int, int);
    var waypoints:seq<(int, int, int)>;
    var state:int;
    var speed:int;

    constructor(height:int, range:int)
    {
        n := height;
        r := range;
        position := (0,0,0);
        state := 0;
        speed := 0;
    }

    function ValidPosition(pos:(int, int, int)) : bool
    reads this`n, this`r
    {
       ((pos.0)*(pos.0) + (pos.1)*(pos.1) + (pos.2 - n)*(pos.2 - n) < r*r) || ((pos.0)*(pos.0) + (pos.1)*(pos.1) <= r*r && pos.2>=0 && pos.2<=n)
    }

    function ValidWayPoint(pos:(int, int, int)) : bool
    reads this`n, this`r
    {
       (pos.0)*(pos.0) + (pos.1)*(pos.1) + (pos.2 - n)*(pos.2 - n) < r*r
    }

    function RepInv() : bool
    reads this`position, this`n, this`r, this`speed, this`state, this`targetpos, this`waypoints 
    {
        ValidPosition(position) && ValidPosition(targetpos) && 0 <= speed <= 10 && 0 <= state <= 3 && forall x :: (0 <= x < |waypoints|) ==> ValidWayPoint(waypoints[x])
    }

    method TakeOff()
    requires RepInv()
    requires ValidPosition((position.0, position.1, n))
    requires state == LANDED
    requires speed == 0
    ensures RepInv()
    ensures state == MOVING 
    ensures speed == MAXSPEED 
    modifies this`targetpos, this`speed, this`state
    {
        targetpos := (position.0, position.1, n);
        speed := MAXSPEED;
        state := MOVING;
    }
    

    method CompleteTakeOff()
    requires RepInv()
    requires state == MOVING
    requires speed == MAXSPEED
    ensures RepInv()
    ensures state == IDLE
    ensures speed == 0
    modifies this`position, this`speed, this`state
    {
        position := targetpos;
        speed := 0;
        state := IDLE;
    }
    
    method Move(dest:(int, int, int))
    requires RepInv()
    requires ValidPosition(dest)
    requires state == IDLE
    requires speed == 0
    ensures RepInv()
    ensures state == MOVING 
    ensures speed == MAXSPEED
    modifies this`targetpos, this`speed, this`state
    {
        targetpos := dest;
        speed := MAXSPEED;
        state := MOVING;
    }

    method CompleteMove()
    requires RepInv()
    requires state == MOVING
    requires speed == MAXSPEED
    ensures RepInv()
    ensures state == IDLE
    ensures speed == 0
    modifies this`position, this`speed, this`state

    {
        position := targetpos;
        speed := 0;
        state := IDLE;
    }

    method AddWaypoint(wayPoint:(int, int, int))
    requires RepInv()
    requires ValidWayPoint(wayPoint)
    requires state == IDLE || state == PATROLLING 
    requires speed == 0 || speed == MAXSPEED/2
    ensures RepInv()
    ensures state == PATROLLING
    ensures speed == MAXSPEED/2
    modifies this`waypoints, this`speed, this`state, this`targetpos
    {
        waypoints := waypoints + [wayPoint];
        targetpos := waypoints[0];
        speed := MAXSPEED/2;
        state := PATROLLING;
    } 

    method NextWaypoint()
    requires RepInv()
    requires state == PATROLLING 
    requires speed == MAXSPEED/2
    requires |waypoints| > 0
    ensures RepInv()
    ensures state == IDLE || state == PATROLLING
    ensures speed == 0 || speed == MAXSPEED/2
    modifies this`waypoints, this`speed, this`state, this`position
    {
        position := targetpos;
        waypoints := waypoints[1..];

        if(|waypoints| == 0){
            speed := 0;
            state := IDLE;
        }
    }

    method Land()
    requires RepInv()
    requires ValidPosition((position.0, position.1, 0))
    requires state == IDLE
    requires speed == 0
    ensures RepInv()
    ensures state == MOVING
    ensures speed == MAXSPEED
    modifies this`targetpos, this`speed, this`state
    {
        targetpos := (position.0, position.1, 0);
        speed := MAXSPEED;
        state := MOVING;
    }

    method CompleteLand()
    requires RepInv()
    requires state == MOVING
    requires speed == MAXSPEED
    ensures RepInv()
    ensures state == LANDED 
    ensures speed == 0
    modifies this`position, this`speed, this`state
    {
        position := targetpos;
        speed := 0;
        state := LANDED;
    }
}